var express = require('express');
var router = express.Router();
var User = require('../models/userModel');
var md5 = require('js-md5');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET login page. */
router.get('/login', function(req, res, next) {
  res.render('login', { layout: 'layout-login'});
});

/* GET login handler. */
router.post('/login', function(req, res, next) {
  
  var email = req.body.email;
  var password = req.body.password;

  User.find({'email':email,'password':md5(password)}, function(error, users){
    
    if(error){
      console.log(JSON.stringify(error));
      res.redirect('/login');
    }else{
      console.log(JSON.stringify(users));
      // valid user and log him in
      if(req.session && users && users.length > 0){
        req.session.isLoggedIn = true;
        req.session.user = users[0];

        //res.locals.session = req.session;

        //res.render('index',{isLoggedIn: req.session.isLoggedIn});
        res.redirect('/');

      }else{
        res.redirect('/login');
      }
    }
  });
});

router.get('/logout', function(req, res, next) {
  if(req.session){
    req.session.user = null;
  }
  res.redirect('/');
});

module.exports = router;
