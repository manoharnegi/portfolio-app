var express = require('express');
var router = express.Router();
var md5 = require('js-md5');
var Client = require('node-rest-client').Client;
var client = new Client();

/* GET users listing. */
router.get('/', function(req, res, next) {
  var myCallback = function(error, data){
    if (error) res.send(error);
    console.log(JSON.stringify(data));
    res.render('userView',{userData: data});
  };

  var req = client.get('http://localhost:3001/users', {}, function (data, response) {
      if (data && data.error == undefined){
        myCallback(null, data.data);
      }else{
        myCallback(data.error, null);
      }
  });

});

router.get('/create',function(req,res,next){
  res.render('createUser');
});

router.post('/create',function(req,res,next){
  var myCallback = function(error, data){
    if (error) res.send(error);
    console.log(JSON.stringify(data));
    res.redirect('/users');
  };

  var args = {
    data: {
      name: req.body.username, 
      password: md5(req.body.password),
      phone: parseInt(req.body.phone),
      email: req.body.email, 
      location: req.body.location
    },
    headers: { "Content-Type": "application/json" }
  };

  client.post('http://localhost:3001/users', args, function (data, response) {
    console.log("data "+ JSON.stringify(data));
    if (data && data.error == undefined){
      myCallback(null, data.data);
    }else{
      myCallback(data.error, null);
    }
  });
});

router.get('/:userid',function(req,res,next){
  var userid = req.params.userid;
  
  var myCallback = function(error, data){
    if (error) res.send(error);
    console.log(JSON.stringify(data));
    res.render('editUser',{user: data});
  };

  var req = client.get('http://localhost:3001/users/'+ userid, {}, function (data, response) {
      if (data && data.error == undefined){
        myCallback(null, data.data);
      }else{
        myCallback(data.error, null);
      }
  });
});

router.post('/:userid',function(req,res,next){
  User.findById(req.params.userid, function(err, user){
    if(err) res.send(err);
    user.name = req.body.username;
    user.phone = parseInt(req.body.phone);
    user.email = req.body.email;
    user.location = req.body.location;
    user.save(function(err){
      if(err) res.send(err);
      res.redirect('/users');
    });
  });
});

router.get('/:userid/delete',function(req,res,next){
  var userid = req.params.userid;
  
  var myCallback = function(error, data){
    if (error) res.send(error);
    console.log(JSON.stringify(data));
    res.redirect('/users');
  };

  var req = client.delete('http://localhost:3001/users/'+ userid, {}, function (data, response) {
      if (data && data.error == undefined){
        myCallback(null, data.data);
      }else{
        myCallback(data.error, null);
      }
  });
});

module.exports = router;

// User.remove({_id: req.params.userid}, function(err, post){
//   if(err) res.send(err);
//   res.json({ message: 'Post deleted!' });
// })