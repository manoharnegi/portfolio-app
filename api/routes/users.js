var express = require('express');
var router = express.Router();
var User = require('../models/user.js');
var md5 = require('js-md5');

/* GET users listing. */
router.get('/', function(req, res, next) {
  User.find({}, function(err, users){
    if (err){
      var jsonResponse = {
        status: 500,
        error: 'Something went wrong'
      };
      res.json(jsonResponse);
    }else{
      console.log(JSON.stringify(users));
      //res.set({ContentType: 'text/json'});
      var jsonResponse = {
        status: 200,
        data: users
      };
      res.json(jsonResponse);
    }
  });
});


/* GET users listing. */
router.post('/', function(req, res, next) {

  console.log(JSON.stringify(req.body));
  var userModel = new User();
  userModel.name = req.body.name;
  userModel.password = md5(req.body.password);
  userModel.phone = parseInt(req.body.phone);
  userModel.email = req.body.email;
  userModel.location = req.body.location;
  userModel.created_at = new Date();
  userModel.save(function(err, user){
    if (err){
      var jsonResponse = {
        status: 500,
        error: 'Something went wrong'
      };
      res.json(jsonResponse);
    }else{
      console.log(JSON.stringify(user));
      //res.set({ContentType: 'text/json'});
      var jsonResponse = {
        status: 200,
        data: user
      };
      res.json(jsonResponse);
    }
  });
});


/* GET user by id. */
router.get('/:userid', function(req, res, next) {
  User.findById(req.params.userid, function(err, user){
    if (err){
      var jsonResponse = {
        status: 500,
        error: 'Something went wrong'
      };
      res.json(jsonResponse);
    }else{
      console.log(JSON.stringify(user));
      //res.set({ContentType: 'text/json'});
      var jsonResponse = {
        status: 200,
        data: user
      };
      res.json(jsonResponse);
    }
  });
});

/* Update user by id. */
router.put('/:userid', function(req, res, next) {
  User.findById(req.params.userid, function(err, user){
    if (err){
      var jsonResponse = {
        status: 500,
        error: 'Something went wrong'
      };
      res.json(jsonResponse);
    }else{
      user.name = req.body.name;
      user.password = md5(req.body.password);
      user.phone = parseInt(req.body.phone);
      user.email = req.body.email;
      user.location = req.body.location;
      user.save(function(err, user1){
        if (err){
          var jsonResponse = {
            status: 500,
            error: 'Something went wrong'
          };
          res.json(jsonResponse);
        }else{
          console.log(JSON.stringify(user1));
          //res.set({ContentType: 'text/json'});
          var jsonResponse = {
            status: 200,
            data: user1
          };
          res.json(jsonResponse);
        }
      });
    }
  });
});

/* Delete user by id. */
router.delete('/:userid', function(req, res, next) {
  User.remove({_id:req.params.userid}, function(err, user){
    console.log(JSON.stringify(user));

    if (err){
      var jsonResponse = {
        status: 500,
        error: 'Something went wrong'
      };
      res.json(jsonResponse);
    }else{
      res.json({status: 200, message: 'User deleted!' });
    }
  });
});
module.exports = router;
